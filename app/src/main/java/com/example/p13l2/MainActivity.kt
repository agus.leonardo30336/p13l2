package com.example.p13l2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var topics = arrayListOf("Berita", "Olahraga", "Gosip")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initSpinner()
        initListener()
    }

    private fun initListener() {
        button.setOnClickListener {
            FirebaseMessaging.getInstance().subscribeToTopic(topics[spinner.selectedItemPosition])
                .addOnCompleteListener { task ->
                    var msg = "Subscribe Success"
                    if (!task.isSuccessful) {
                        msg = "Subscribe Failed"
                    }
                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                }
        }
        button2.setOnClickListener {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topics[spinner.selectedItemPosition])
                .addOnCompleteListener { task ->
                    var msg = "Unsubscribe Success"
                    if (!task.isSuccessful) {
                        msg = "Unsubscribe Failed"
                    }
                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                }
        }
    }

    private fun initSpinner() {
        val dataAdapter =
            ArrayAdapter(this, android.R.layout.simple_spinner_item, topics)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = dataAdapter
    }

}
