package com.example.p13l2

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessageingService : FirebaseMessagingService() {
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        if (p0.notification != null) {
            showNotify(
                p0.notification!!.title,
                p0.notification!!.body
            )
        }
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        Log.w("TOKEN FIREBASE", p0)
    }

    private fun showNotify(title: String?, body: String?) {
        val myNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val myNotificationChannel = NotificationChannel(
                CHANNEL_1_ID,
                "Notification",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            myNotificationChannel.description = "FCM Channel 1"
            myNotificationChannel.enableLights(true)
            myNotificationChannel.lightColor = Color.WHITE
            myNotifyManager.createNotificationChannel(myNotificationChannel)
        }

        val myNotify = NotificationCompat
            .Builder(this, CHANNEL_1_ID)
            .setDefaults(Notification.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(body)
            .setContentInfo("Info")

        myNotifyManager.notify(1, myNotify.build())

    }

    companion object {
        val CHANNEL_1_ID = "channel1"

    }
}

